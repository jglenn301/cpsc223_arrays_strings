#include <string.h>
#include <stdio.h>

/**
 * Reads from standard input into the given array until a newline character
 * is read.  The array is null-terminated.  The newline character is removed
 * from standard input.  If more than max characters appear before the next
 * newline then the excess are discarded.
 *
 * @param s an array of characters of size at least max + 1
 * @param max a nonnegative integer
 */
void read_line(char s[], int max);

int main()
{
  // strings are null-terminated arrays of characters
  char given_name[] = {'J', 'a', 'm', 'e', 's', '\0'};
  char school[] = "yale"; // compiler detects size (5 -- 4 chars + null)
  //char given_name[] = "James"; // equivalent to above
  char family_name[10] = "Glenn"; // extra space uninitialized
  
  char department[] = "cs";
  char empty[3] = "";
  char full_name[strlen(given_name) + strlen(family_name) + 3];
  //strcat(given_name, "what is going to happen here?");
  strcpy(full_name, family_name);
  strcat(full_name, ", ");
  strcat(full_name, given_name);

  char copy[strlen(full_name) + 1];
  strcpy(copy, full_name);

  printf("%ld\n", strlen(full_name));
  printf("%s\n", copy);

  // DONT DO THIS  VERY DANGEROUS!!!
  //scanf("%s", given_name);

  char ai_name[20];
  printf("What is my name?\n");
  read_line(ai_name, 19);
  printf("I don't like %s, can I choose a different name?\n", ai_name);
}

void read_line(char s[], int max)
{
  int count = 0;
  int ch;
  while ((ch = getchar()) != EOF && ch != '\n')
    {
      if (count < max)
	{
	  s[count] = ch;
	}
      count++;
    }
  if (count > max)
    {
      s[max] = '\0';
    }
  else
    {
      s[count] = '\0';
    }
}

  
  
