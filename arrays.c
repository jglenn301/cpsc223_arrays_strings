#include <stdio.h>

/**
 * Computes the sum of the elements in the given array.
 *
 * @param n a nonnegative integer
 * @param arr an array of n ints
 * @return the sum of the elements in arr
 */
int sum(int n, int arr[]);

/**
 * Computes the sum of the elements in the given 2-D array.
 *
 * @param n a nonnegative integer
 * @param m a nonnegative integer
 * @param arr an n row, m column 2-D array of ints
 * @return the sum of the elements in arr
 */
int sum2D(int n, int m, int arr[][m]);

/**
 * Stores zero in each element of the given array.
 *
 * @param n a nonnegative integer
 * @param arr an array of n ints
 */
void zero(int n, int arr[]);

/**
 * Stores zero in each element of the given array.
 *
 * @param n a nonnegative integer
 * @param m a nonnegative integer
 * @param arr an n row, m column 2-D array of ints
 */
void zero2D(int n, int m, int arr[][m]);

int main()
{
  int a[5] = {12, 24, 45, 99, 100};

  int a2d[3][3] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
  int b2d[3][4];

  zero2D(3, 4, b2d);

  printf("%d\n", sum(5, a));
  printf("%d\n", sum(3, a2d[1])); // row of 2-D array is a 1-D array
  printf("%d\n", sum(4, a2d[1])); // lying about size -- wraps around to next row 
  printf("%d\n", sum(3, b2d[0])); // sum is zero, of course, since we zeroed it with zero2D
  printf("%d\n", sum2D(3, 4, b2d));
}

void zero2D(int n, int m, int arr[][m])
{
  for (int r = 0; r < n; r++)
    {
      for (int c = 0; c < m; c++)
	{
	  arr[r][c] = 0;
	}
    }
}

void zero(int n, int arr[]) // as a parameter, int arr[] is equiv to int *arr
{
  for (int i = 0; i < n; i++)
    {
      arr[i] = 0;
    }
}

int sum(int n, int arr[])
{
  int sum = 0;
  for (int i = 0; i < n; i++)
    {
      sum += arr[i];
    }
  return sum;
}

int sum2D(int n, int m, int arr[][m])
{
  int total = 0;
  for (int i = 0; i < n; i++)
    {
      total += sum(m, arr[i]);
    }
  return total;
}
